import json
import data_flow_utils
from logging_utils import set_logger, get_logs_list, logger, clear_log
from snowflake.snowpark import Session
from snowflake.snowpark.functions import col


# Method to generate query for creating warehouse
def process_warehouse_object(ss, object_data: dict, deploy_attr_stream_dict: dict):
    """
    Create warehouse with the provided warehouse properties.

    Args:
        ss (Snowpark Session): snowpark session object
        warehouse_attributes (dataframe): dataframe of warehouse attributes
        :param ss:
        :param deploy_attr_stream_dict:
        :param object_data:

    """

    def create_warehouse(wh_attr: dict):
        wh_properties = json.loads(wh_attr['COMPUTE_WH_PROPERTIES'])
        actual_query = f"CREATE OR REPLACE WAREHOUSE {wh_attr['COMPUTE_WH_NAME']}"
        query = ""

        if wh_properties is not None:
            for key, value in wh_properties.items():
                if key.lower() == 'comment':
                    query = query + f" {key}='{str(value)}'"
                else:
                    query = query + f" {key}={str(value)}"
            warehouse_query = actual_query + " WITH " + query + ""
        else:
            warehouse_query = actual_query + ""
        ss.sql(warehouse_query).collect()

    warehouse_exists = session.sql(f"show warehouses like '{object_data['COMPUTE_WH_NAME']}'").count()

    if deploy_attr_stream_dict['CREATE_WH_FG']:
        warehouse_attributes = session.table('warehouse_attr') \
            .filter(col('compute_wh_name') == object_data['COMPUTE_WH_NAME']).first().as_dict()

        create_warehouse(warehouse_attributes)

        if warehouse_exists == 0:
            logger.info(f"Created a new warehouse {object_data['COMPUTE_WH_NAME']}")
        else:
            logger.info(f"Updating existing warehouse {object_data['COMPUTE_WH_NAME']}")
    else:
        if warehouse_exists == 0:
            raise Exception(
                f"The `CREATE_WH_FG` flag in the deploy attribute table is set to False, "
                f"and the warehouse {object_data['COMPUTE_WH_NAME']} does not exist. "
                f"This will impact data processing. Please create the warehouse "
                f"and re-initiate the metadata deployment process.")
    return


# Methods to generate query for creating stream
def get_stream_statement(stream_prop) -> str:
    """
    Generate statement with the provided stream properties.

    Args:
        stream_prop (dict): dictionary of stream properties

    Returns:
        str: resolved query statement

    """
    statement = ""
    for k, v in stream_prop.items():
        if k.lower() == 'comment':
            statement = statement + f" {k}='{str(v)}'"
        else:
            statement = statement + f" {k}={str(v)}"
    return statement


def process_stream_object(ss, object_data: dict, deploy_attr_stream_dict: dict):
    """
    Create stream with the provided stream properties.

    Args:
        ss (Snowpark Session): snowpark session object
        object_data (dataframe): dataframe of object attributes

    Raise:
        Exception: if object_type is other than Table/View
        :param ss:
        :param object_data:
        :param deploy_attr_stream_dict:

    """

    def create_stream():
        stream_prop = json.loads(object_data['STREAM_PROPERTIES'])
        stream_ddl_statement = \
            f"CREATE OR REPLACE STREAM " \
            f"{object_data['DATABASE_NAME']}.{object_data['SCHEMA_NAME']}.{object_data['STREAM_NAME']} ON TABLE "

        if object_data['OBJECT_TYPE'].lower() == 'table':
            stream_ddl_statement += \
                str(f"{object_data['DATABASE_NAME']}.{object_data['SCHEMA_NAME']}.{object_data['OBJECT_NAME']}")
        elif object_data['OBJECT_TYPE'].lower() == 'view':
            stream_ddl_statement += \
                str(f"{object_data['DATABASE_NAME']}.{object_data['SCHEMA_NAME']}.{object_data['PRIMARY_TABLE']}")
        else:
            raise Exception("Invalid object type.")

        if stream_prop is None:
            create_stream_query = stream_ddl_statement + " append_only = True"
            logger.info("Stream properties not provided; "
                        "creating the stream with default configuration (Append_Only=True).")
        else:
            if 'time_travel' in [key.lower() for key in stream_prop.keys()]:
                tt_statement = ""
                for key, value in stream_prop['time_travel'].items():
                    tt_statement = tt_statement + " " + key + "(" + value + ")"
                stream_prop.pop('time_travel')
                stream_prop_statement = tt_statement + get_stream_statement(stream_prop)
            else:
                stream_prop_statement = get_stream_statement(stream_prop)

            create_stream_query = stream_ddl_statement + stream_prop_statement + ""

        ss.sql(create_stream_query).collect()

    stream_exists = \
        session.sql(
            f"show streams like '{object_data['STREAM_NAME']}' "
            f"in {object_data['DATABASE_NAME']}.{object_data['SCHEMA_NAME']}"
        ).count()

    if stream_exists == 0:
        create_stream()
        logger.info(f"Created new stream {object_data['STREAM_NAME']}")
    else:
        if deploy_attr_stream_dict['REPLACE_EXISTING_STREAM']:
            create_stream()
            logger.info(f"Successfully updated stream {object_data['STREAM_NAME']}.")
        else:
            logger.info(
                f"The stream {object_data['STREAM_NAME']} already exists and "
                f"'replace_existing_stream' is set to False in deploy attribute table. "
                f"No changes were made to the existing stream.")
    return


# Entry point of the script
def metadata_generation(session, log_level):
    """
    Generate metadata using the information from metadata tables.

    Parameters:
    session (Session): A Snowpark session
    log_level (str): Log level for logger (INFO, DEBUG, WARNING, ERROR)

    Returns:
    None
    """

    process_name = ''
    deploy_status = ''
    set_logger(log_level)

    logger.info("Reading data from deploy attr table stream")
    deploy_attr_stream = session.table('deploy_attr_stream').collect()

    if deploy_attr_stream is None:
        logger.warning("Deploy attribute stream is empty")
    else:
        # Looping through the deploy attribute stream.
        for record in deploy_attr_stream:
            try:
                deploy_attr_stream_dict = record.as_dict()
                process_name = deploy_attr_stream_dict['PROCESS_NAME']

                logger.info(f"Retrieved record from deploy attribute stream. "
                            f"Deploying metadata configuration for `{process_name}` ...")

                # Check if there is a duplicate entry for the process in the object attributes table,
                # otherwise retrieve the data from the table
                object_attr_data = session.table("object_attr").filter(col('process_name') == process_name)

                if object_attr_data.count() > 1:
                    raise Exception(f"Duplicate entry found in Object Attributes for the process {process_name}. "
                                    f"Please check the configuration and try again.")
                else:
                    # Retrieve the data from the table and log the info message
                    object_data = object_attr_data.first().as_dict()
                    logger.info(f"Retrieved object attributes for the process {process_name}")

                # Creating warehouse if create warehouse flag is enabled
                process_warehouse_object(session, object_data, deploy_attr_stream_dict)

                # Creating stream if incremental_capture_flag is enabled
                if object_data['INCREMENTAL_CAPTURE_FLAG']:
                    process_stream_object(session, object_data, deploy_attr_stream_dict)

                # Constructing data pipeline config
                pipeline_config = data_flow_utils.construct_pipeline_json(session, process_name, object_data)

                # Persist data pipeline into pipeline table
                pipeline_exists = session.table("pipeline").filter(col('PROCESS_NAME') == process_name).first()

                if not pipeline_exists:
                    session.sql(
                        f"INSERT INTO PIPELINE "
                        f"(PROCESS_NAME, PIPELINE_CONFIG, RUNTIME_PARAMS, COMMENT_TEXT, CREATION_TS, UPDATE_TS) "
                        f"SELECT '{process_name}'"
                        f",parse_json($${pipeline_config}$$)"
                        f",NULL"
                        f",'{deploy_attr_stream_dict['COMMIT_MSG']}'"
                        f",current_timestamp()"
                        f",current_timestamp()"
                    ).collect()
                    logger.info(f"Pipeline configuration has been inserted for the process '{process_name}'.")
                else:
                    session.sql(
                        f"UPDATE PIPELINE "
                        f"SET PIPELINE_CONFIG = parse_json($${pipeline_config}$$)"
                        f", UPDATE_TS= current_timestamp() "
                        f", COMMENT_TEXT = '{deploy_attr_stream_dict['COMMIT_MSG']}'"
                        f"WHERE PROCESS_NAME='{process_name}'"
                    ).collect()
                    logger.info(f"Updated pipeline configuration for process {process_name} in pipeline table")

                logger.info(f"Metadata generation has been completed successfully for process {process_name}")
                deploy_status = 'SUCCEEDED'
            except Exception as e:
                logger.error(f"Metadata generation failed for process {process_name}: {e}. "
                             f"Please check deploy_attr for more details.")
                deploy_status = 'FAILED'
            finally:
                # get the logs and update deploy attributes table
                final_log_list = get_logs_list()

                session.sql(
                    f"UPDATE DEPLOY_ATTR "
                    f"SET STATUS = '{deploy_status}'"
                    f",LOG_MESSAGE = $${final_log_list}$$"
                    f", COMPLETION_TS = current_timestamp()  "
                    f"where PROCESS_NAME='{process_name}'"
                ).collect()

                clear_log()
        else:
            session.sql("create temporary table deploy_attr_stream_temp as select * from deploy_attr_stream").collect()

    return


if __name__ == '__main__':
    config_path = "resources/session_config.json"
    f = open(config_path, "r")
    connection_parameters = json.loads(f.read())
    session = Session.builder.configs(connection_parameters).create()
    metadata_generation(session, log_level='INFO')
