import json
from snowflake.snowpark.functions import col
from logging_utils import logger
import dynamic_view_query_generation


def set_db_context(object_data):
    """
    Data flow step to set context.

    Args:

        object_data (dataframe): dataframe of object attributes

    Returns:
        dict: steps for setting database context

    """
    db_context = {
        "desc": "set the database,schema and warehouse",
        "func": "ddl",
        "query_list": [f"use {object_data['DATABASE_NAME']};", f"use schema {object_data['SCHEMA_NAME']};",
                       f"use WAREHOUSE {object_data['COMPUTE_WH_NAME']};"]
    }
    return db_context


def consume_stream(object_data):
    """
    Data flow step to consume stream.

    Args:

        object_data (dataframe): dataframe of object attributes

    Returns:
        dict: steps for consuming stream

    """
    if object_data['OBJECT_TYPE'].lower() == 'view':
        stream_query = f"create transient table if not exists {object_data['PRIMARY_TABLE']}_stg " \
                       f"as SELECT * FROM {object_data['STREAM_NAME']}"
    else:
        stream_query = f"create transient table if not exists {object_data['OBJECT_NAME']}_stg " \
                       f"as SELECT * FROM {object_data['STREAM_NAME']}"

    read_stream = {
        "desc": "consume stream by loading data into transient table",
        "func": "consume_stream",
        "stream_name": object_data['STREAM_NAME'],
        "stream_query": stream_query
    }
    return read_stream


def create_dynamic_view(session, view_name, primary_table):
    """
    Data flow step to create dynamic view.

    Args:
        session (Snowpark Session): snowpark session object
        view_name (str): name of the view to be created
        primary_table(str): name of the driving table for view

    Returns:
        dict: steps for creating dynamic view

    """
    view_query = dynamic_view_query_generation.create_dynamic_view_query(session, view_name, primary_table)

    dynamic_view_query = {
        "desc": "dynamic view query generation",
        "func": "ddl",
        "query": view_query
    }

    return dynamic_view_query


def extract_data(object_data):
    """
    Data flow step to extract data from transient table.

    Args:
        object_data (dataframe): dataframe of object attributes

    Returns:
        dict: steps for extracting data

    """
    if object_data['OBJECT_TYPE'].lower() == 'view':
        extract_query = f"select * from {object_data['OBJECT_NAME']}"
    else:
        extract_query = f"select * from {object_data['OBJECT_NAME']}_stg"
    data_extract = {
        "desc": "create dataframe extracting data from transient table",
        "func": "sql",
        "query": extract_query,
        "output_df": "incr_df"
    }
    return data_extract


def mp_sink(column_mapping, select_exp_list, target_prop):
    """
    Data flow step to construct payload for mparticle.

    Args:
        column_mapping (dict): dictionary of target columns grouped by  mparticle object categories
        select_exp_list (list): list containing source and target column mapping

    Returns:
        dict: steps for constructing mparticle payload

    """
    load_mp_sink = {
        "desc": "convert dataframe to payload and send data to mparticle",
        "func": "mp_sink",
        "input_df": "incr_df",
        "select_exp_listr": select_exp_list,
        "mp_properties": target_prop,
        "column_mapping": column_mapping
    }
    return load_mp_sink


def drop_transient_table(object_data):
    """
    Data flow step to Drop the transient table.

    Args:
        object_data (dataframe): dataframe of object attributes

    Returns:
        dict: steps to drop transient table

    """

    if object_data['OBJECT_TYPE'].lower() == 'view':
        drop_query = f"drop table {object_data['PRIMARY_TABLE']}_stg"
    else:
        drop_query = f"drop table {object_data['OBJECT_NAME']}_stg"

    drop_trans_table = {
        "desc": "Drop the transient table",
        "func": "ddl",
        "query": drop_query
    }
    return drop_trans_table


def column_mapping(column_attributes, target):
    """
        Create Column mapping from source to target.

    Args:

        column_attributes (dataframe): dataframe of column attributes
        target(str): Name of the target

    Returns:
        list: list containing source and target column mapping
        dict: dictionary of target columns grouped by  mparticle object categories

    Raises:
          Exception: if column attributes for corresponding target are not found

    """
    col_attr_df = column_attributes.filter(col('TARGET_NAME') == target).collect()

    if col_attr_df:
        column_map = {}
        select_expr = []
        for record in col_attr_df:
            column_attr = record.as_dict()
            if column_attr['TARGET_COLUMN_NAME']:
                if column_attr['OBJECT_CATEGORY'] in column_map.keys():
                    object_category = column_attr['OBJECT_CATEGORY']
                    target_column_name = column_attr['TARGET_COLUMN_NAME']
                    column_name = target_column_name + "_" + object_category
                    column_map[object_category][column_name] = target_column_name
                else:
                    column_name = f"{column_attr['TARGET_COLUMN_NAME']}_{column_attr['OBJECT_CATEGORY']}"
                    column_map[column_attr['OBJECT_CATEGORY']].update({column_name: column_attr['TARGET_COLUMN_NAME']})

                if column_attr['TRANSFORM_RULE']:
                    select_expr.append(
                        f"{column_attr['TRANSFORM_RULE']} AS "
                        f"{column_attr['TARGET_COLUMN_NAME']}_{column_attr['OBJECT_CATEGORY']}"
                    )
                else:
                    select_expr.append(
                        f"{column_attr['COLUMN_NAME']} AS "
                        f"{column_attr['TARGET_COLUMN_NAME']}_{column_attr['OBJECT_CATEGORY']}"
                    )

    else:
        raise Exception(f"Column attributes not found for the target: {target}")

    return column_map, select_expr


def construct_data_flow(session, process_name, object_data):
    """
    Construct dataflow list.

    Args:
        session (Snowpark Session): snowpark session object
        process_name (str): name of the process for which metadata generation is triggered
        object_data (dataframe): dataframe of object attributes]

    Returns:
        list: list containing steps for data flow

    Raises:
          Exception: if target properties for corresponding target are not found

    """
    # Get data from column attributes table
    column_attributes = session.table('column_attr').filter(col('process_name') == process_name)

    data_flow_list = [set_db_context(object_data)]

    if object_data['INCREMENTAL_CAPTURE_FLAG']:
        data_flow_list.append(consume_stream(object_data))
    else:
        pass

    if object_data['OBJECT_TYPE'].lower() == 'view':
        data_flow_list.append(create_dynamic_view(session, object_data['OBJECT_NAME'], object_data['PRIMARY_TABLE']))
    else:
        pass

    data_flow_list.append(extract_data(object_data))

    targets_list = json.loads(object_data['TARGET_NAME_LIST'])

    for target in targets_list:
        if target.lower() == 'mparticle':
            # get data from target attributes table
            target_attr = \
                session.table('target_attr')\
                    .filter(col('process_name') == process_name)\
                    .filter(col('TARGET_NAME') == target)

            if target_attr is not None:
                target_attr_dict = target_attr.first().as_dict()
                target_prop = target_attr_dict['TARGET_PROPERTIES']
                logger.info(f"Target attributes retrieved for {process_name}")
            else:
                raise Exception(f'Target attributes not found for {process_name}')

            col_map, select_exp_list = column_mapping(column_attributes, target)
            data_flow_list.append(mp_sink(col_map, select_exp_list, target_prop))
        else:
            raise Exception(f'Invalid target {target}.Only mparticle is supported currently')

    data_flow_list.append(drop_transient_table(object_data))

    return data_flow_list


def construct_pipeline_json(session, process_name, object_data):
    # Construct data flow json
    data_flow = construct_data_flow(session, process_name, object_data)

    logger.info(f"Data Flow JSON successfully created for process '{process_name}'")

    # Get data from email attributes table if configured in object_attr
    email_attributes = {}
    if object_data['EMAIL_INTG_NAME'] is not None:
        email_attributes = session.table('email_attr').filter(
            col('email_intg_name') == object_data['EMAIL_INTG_NAME'])
        logger.info(f"Retrieved email attributes for process '{process_name}'")
    else:
        logger.info(f"No email integration configured for process '{process_name}'")
    email_attr = email_attributes.collect()[0].as_dict()

    # construct pipeline JSON
    pipeline_metadata = {
        "root": {
            "pipeline_name": object_data['PROCESS_NAME'],
            "desc": object_data['PROCESS_DESC'],
            "email_intg_name": email_attr['EMAIL_INTG_NAME'],
            "email_addr_list": email_attr['EMAIL_ADDR_LIST'],
            "data_flow": data_flow
        }
    }
    pipeline_metadata_json = json.dumps(pipeline_metadata)

    return pipeline_metadata_json
