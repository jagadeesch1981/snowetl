import logging
import sys
from io import StringIO
logs_list = StringIO()
# Get the logger instance
logger = logging.getLogger("snowflake.snowpark")


def set_logger(log_level: str):
    """
    This function sets up the logger and takes in two arguments:
    log_level : str : The desired log level (e.g. "DEBUG", "INFO", etc)
    logs_list : io.StringIO : An instance of io.StringIO to capture the logs
    """
    # Get the log level constant corresponding to the log level name
    log_level = logging.getLevelName(log_level)
    # Set the log level for the logger
    logger.setLevel(log_level)
    # Create a stream handler for the logs_list variable
    ch = logging.StreamHandler(logs_list)
    ch.setLevel(log_level)
    # Set the log message formatter
    formatter = logging.Formatter("%(asctime)s - %(threadName)s %(filename)s:%(lineno)d - %(funcName)s() - %("
                                  "levelname)s - %(message)s")
    ch.setFormatter(formatter)
    # Add the stream handler to the logger
    logger.addHandler(ch)
    # Create a stream handler for stdout
    ch1 = logging.StreamHandler(sys.stdout)
    ch1.setLevel(log_level)
    ch1.setFormatter(formatter)
    logger.addHandler(ch1)


def get_logs_list():
    """
    This function returns the logs captured in the logs_list variable
    logs_list : io.StringIO : An instance of io.StringIO that contains the logs
    """
    return logs_list.getvalue()


def clear_log():
    """
    This function clears the logs captured in the logs_list variable
    logs_list : io.StringIO : An instance of io.StringIO that contains the logs
    """
    return logs_list.truncate(0)